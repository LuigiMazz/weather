//
//  ViewController.swift
//  Weather
//
//  Created by Luigi Mazzarella on 09/12/2019.
//  Copyright © 2019 Luigi Mazzarella. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate,UITableViewDelegate {
    
    //MARK: - Properties
    fileprivate var forecast: ForecastAPI? {
        
        // L'ho strutturato in modo che quando viene cambiata la location e viene settato questo forecast perché l'API l'ha trovato, si fa tutte queste operazioni. L'ho messo in un DispatchQueue.main perché il codice per modificare le cose della UI sono da eseguire sul thread principale
        didSet {
            DispatchQueue.main.async {
                guard let forecastInfo = self.forecast else { return  }
                self.cityNameLabel.text = forecastInfo.city.name
                self.conditionImage.image = UIImage(named: forecastInfo.list[0].weather[0].icon)
                self.weatherConditionLabel.text = forecastInfo.list[0].weather[0].weatherDescription
                self.temperatureLabel.text = "\(forecastInfo.list[0].main.temp)°"
                self.cityHumidity.text = forecastInfo.city.name // Da sostituire con il giorno esatto della settimana
                self.tempMinium.text = "\(forecastInfo.list[0].main.tempMin)°"
                self.tempMax.text = "\(forecastInfo.list[0].main.tempMax)°"
                self.HumidityLabel.text = "Humidity: \(forecastInfo.list[0].main.humidity)%"
                
                let suffix = forecastInfo.list[0].weather[0].icon.suffix(1)
                if(suffix == "n"){
                    self.setGreyGradientBackground()
                }else{
                    self.setBlueGradientBackground()
                }
            }
        }
    }
    
    // List è l'insieme di informazioni sul meteo nelle varie fasce orarie
    fileprivate var list: [List] = []
    
    let apiKey = "afd0e7890f98c3e219dd9520b1722ea9"
    var lat = 41.0842
    var lon = 14.3358
    
    //MARK: - IBOutlets
    @IBOutlet weak var weatherTableView: UITableView!
    @IBOutlet weak var cityHumidity: UILabel!
    @IBOutlet weak var lineHumidity: UIView!
    @IBOutlet weak var conditionImage: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var weatherConditionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var tempMax: UILabel!
    @IBOutlet weak var tempMinium: UILabel!
    @IBOutlet weak var HumidityLabel: UILabel!
    
    let locationManager = CLLocationManager()
    let gradientLayer = CAGradientLayer()
    
    //MARK: - Life Cycle Management
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundView.layer.addSublayer(gradientLayer)
        
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        setBlueGradientBackground()
        setGreyGradientBackground()
    }
    
    
    //MARK: - Location Manager Methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        lat = locValue.latitude
        lon = locValue.longitude
        print(lat,lon)
        fetchCurrentWeatherData()
    }
    
    
    
    //MARK: - API Methods
    func fetchCurrentWeatherData() {
        let key = "afd0e7890f98c3e219dd9520b1722ea9"
        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: OperationQueue.main)
        let endpoint = "https://api.openweathermap.org/data/2.5/forecast?lat=\(lat)&lon=\(lon)&units=metric&appid=\(key)"
        let safeURLString = endpoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        guard let endpointURL = URL(string: safeURLString!) else {
            print("The URL is invalid")
            return
        }
        
        var request = URLRequest(url: endpointURL)
        request.httpMethod = "GET"
        
        //The datatask can also be run the the URL directly if no specific configuration is required
        //response and error are not used in the closure, this could be improved to provide feedback to users.
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            
            guard let jsonData = data else {
                print("The payload is invalid")
                return
            }
            let decoder = JSONDecoder()
            do {
                
                let forecastInfo = try decoder.decode(ForecastAPI.self, from: jsonData)
                print("weather JSON decoded")
                // Quando il JSON è stato decodificato, setta sia forecast che list, che sono le due variabili all'inizio del view controller
                self.forecast = forecastInfo
                self.list = forecastInfo.list
                
                self.weatherTableView.reloadData()
                
     
            } catch let error {
                print(error)
            }
        }
        dataTask.resume()
    }
    
    
    //MARK: - Gradient setup
    func setBlueGradientBackground(){
        let topColor = UIColor(red: 95.0/255.0, green: 165.0/255.0, blue: 1.0, alpha: 1.0).cgColor
        let bottomColor = UIColor(red: 72.0/255.0, green: 114.0/255.0, blue: 184.0/255.0, alpha: 1.0).cgColor
        
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [topColor, bottomColor]
    }
    
    func setGreyGradientBackground(){
        let topColor = UIColor(red: 151.0/255.0, green: 151.0/255.0, blue: 151.0/255.0, alpha: 1.0).cgColor
        let bottomColor = UIColor(red: 72.0/255.0, green: 72.0/255.0, blue: 72.0/255.0, alpha: 1.0).cgColor
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [topColor, bottomColor]
    }
    
    
}


//MARK: - Table View Data Source

extension ViewController: UITableViewDataSource {
    
    // Il numero di righe della table view deve essere pari al numero di informazioni per ora che mi ha restituito l'API
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    
    /*
     Mi sono fatto un metodo nella cella chiamato configure in modo che gli passo il dato e lui internamente si va a settare la Label con quel testo.
     Questo ti semplifica un sacco, perché se tu ora devi rendere la cella più complessa, basta che cambi il configure così da passargli l'intero oggetto con la temperatura minima, massima, ecc, e lui se lo configura da solo, senza rendere questo metodo troppo lungo e complesso (tra l'altro potrebbe darti problemi cambiare l'interfaccia grafica da qui, a me li dava, diceva che trovava le varie view come nil)
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.weatherTableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as? TableViewCell else { return UITableViewCell() }
        
        cell.configure(dateString: list[indexPath.row].dtTxt, tempMinString: "\(list[indexPath.row].main.tempMin)°", tempMaxString: "\(list[indexPath.row].main.tempMax)°", imageName: UIImage(named: list[indexPath.row].weather[0].icon)!)
        
        
        return cell
    }
    
}




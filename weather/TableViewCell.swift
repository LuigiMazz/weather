//
//  TableViewCell.swift
//  weather
//
//  Created by Luigi Mazzarella on 12/12/2019.
//  Copyright © 2019 Luigi Mazzarella. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var condImage: UIImageView!
    @IBOutlet weak var temperatureMiniumCell: UILabel!
    @IBOutlet weak var temperatureMaximiumCell: UILabel!
    @IBOutlet weak var date: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(dateString: String, tempMinString: String, tempMaxString: String,imageName: UIImage) {
        self.date.text = dateString
        self.temperatureMiniumCell.text = tempMinString
        self.temperatureMaximiumCell.text = tempMaxString
        self.condImage.image = imageName
    }

}
